terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
  token     = "y0_AgAAAABqT1WnAATuwQAAAADiI_aB6YwmclAmR9aXAPWsDf3Bkut5-lk"
  cloud_id  = "b1go4n53hofro0ufj8ji"
  folder_id = "b1gj2tad8qm148k1ihre"
  zone      = "ru-central1-a"
}

resource "yandex_compute_instance" "my-vm-2" {
  name        = "vm-monitoring"
  platform_id = "standard-v1"
  zone        = "ru-central1-a"

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = "fd8auu58m9ic4rtekngm"
    }
  }

  network_interface {
    subnet_id = "e9b78clibh11foif5bmi"
    nat = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file("/home/kirill/.ssh/id_ed25519.pub")}"
  }
}

resource "yandex_dns_recordset" "rs2" {
  zone_id = "dns8ft4tl9vm9u7au4hs"
  name    = "monitoring.kirill-mas.ru."
  type    = "A"
  ttl     = 200
  data    = [(yandex_compute_instance.my-vm-2.network_interface.0.nat_ip_address)]
}

resource "yandex_dns_recordset" "rs3" {
  zone_id = "dns8ft4tl9vm9u7au4hs"
  name    = "grafana.kirill-mas.ru."
  type    = "A"
  ttl     = 200
  data    = [(yandex_compute_instance.my-vm-2.network_interface.0.nat_ip_address)]
}

output "internal_ip_address_vm_2" {
  value = yandex_compute_instance.my-vm-2.network_interface.0.ip_address
}

output "external_ip_address_vm_2" {
  value = yandex_compute_instance.my-vm-2.network_interface.0.nat_ip_address
}
